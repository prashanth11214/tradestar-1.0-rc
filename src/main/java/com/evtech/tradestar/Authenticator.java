package com.evtech.tradestar;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;

import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.file.FileSystemException;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;

import sun.misc.BASE64Decoder;

public class Authenticator extends Verticle {

	EventBus eb;
	JsonObject userAuthenticateResult;
	JsonObject userRegisterResult;
	JsonObject userUpdateResult;
	BASE64Decoder decoder;
	DatabaseManager dbManager;
	PasswordHash passwordHash;
	SessionManager sessionManager;
	Map<String, String> sessions;

	Object sync;

	boolean foundUsername;
	boolean foundEmail;

	public void start() {

		sessions = vertx.sharedData().getMap("sessions");

		dbManager = DatabaseManager.getInstance();

		sessionManager = new SessionManager(vertx);

		eb = vertx.eventBus();
		decoder = new BASE64Decoder();

		System.out.println("authenticator online");

		Handler<Message<JsonObject>> authenticator = new Handler<Message<JsonObject>>() {
			public void handle(Message<JsonObject> message) {
				JsonObject request = message.body();
				String action = request.getString("action");

				container.logger().info("Inside authenticator handler with " + request.encodePrettily());

				if (action.equals("authenticate")) {
					System.out.println("authenticator.handle with action authenticate");
					if (validAuthenticateUserRequest(request)) {
						try {
							message.reply(authenticate(request));
						} catch (InvalidKeySpecException e) {
							e.printStackTrace();
						} catch (NoSuchAlgorithmException e) {
							e.printStackTrace();
						}
					} else
						message.reply(new JsonObject().putString("status", "error").putString("message",
								"Invalid authenticate user request"));
				} else if (action.equals("register")) {
					if (validRegisterUserRequest(request)) {
						try {
							message.reply(register(request));
						} catch (InvalidKeySpecException e) {
							e.printStackTrace();
						} catch (NoSuchAlgorithmException e) {
							e.printStackTrace();
						}
					} else
						message.reply(new JsonObject().putString("status", "error").putString("message",
								"Invalid register user request"));
				} else if (action.equals("update")) {
					if (validUpdateUserRequest(request)) {
						message.reply(update(request));
					} else
						message.reply(new JsonObject().putString("status", "error").putString("message",
								"Invalid update user request"));

				} else if (action.equals("resume")) {
					if (validResumeSessionRequest(request)) {
						message.reply(resume(request));
					} else
						message.reply(new JsonObject().putString("status", "error").putString("message",
								"Invalid resume session request"));
				} else
					message.reply(new JsonObject().putString("status", "error").putString("message",
							"Invalid Authenticator action"));
			}
		};

		eb.registerHandler("Authenticator", authenticator);
	}

	public boolean validAuthenticateUserRequest(JsonObject authenticateUserRequest) {
		// More validation code in the future?
		if (authenticateUserRequest.containsField("username") && authenticateUserRequest.containsField("password")) {
			if (authenticateUserRequest.getString("username").trim().equals("")
					|| authenticateUserRequest.getString("password").trim().equals(""))
				return false;
			return true;
		} else
			return false;
	}

	public JsonObject authenticate(JsonObject user) throws InvalidKeySpecException, NoSuchAlgorithmException {
		System.out.println("Authenticator.authenticate() for user " + user.encodePrettily());

		JsonArray results = new JsonArray();
		JsonObject userAuthenticateRequest = new JsonObject();
		String username = user.getString("username").trim();
		String password = user.getString("password").trim();

		userAuthenticateRequest.putString("collection", "users")
				.putObject("keys", new JsonObject().putNumber("password", 1))
				.putObject("matcher", new JsonObject().putString("username", username));

		results = dbManager.find(userAuthenticateRequest).getArray("results");

		if (results.size() <= 0) {
			return new JsonObject().putString("status", "error").putString("message", "Invalid username or password");
		}

		String hashedPassword = ((JsonObject) results.get(0)).getString("password");

		if (!passwordHash.validatePassword(password, hashedPassword)) {
			return new JsonObject().putString("status", "error").putString("message", "Invalid username or password");

		} else {
			JsonObject newSession = sessionManager.startSession(username);
			System.out.println("newSession " + newSession);
			if (newSession.getString("status").equals("ok")) {

				JsonObject playerDataReq = new JsonObject().putString("collection", "users").putObject("matcher",
						new JsonObject().putString("username", username));

				JsonArray playerData = dbManager.find(playerDataReq).getArray("results");

				JsonObject playerDataAsJsonObject = playerData.get(0);
				System.out.println("DB RESULT: " + playerDataAsJsonObject.toString());

				eb.send("ConnectPlayer",
						playerDataAsJsonObject.putString("sessionID", newSession.getString("sessionID")));

				Map<String, Object> playerInfo = playerDataAsJsonObject.toMap();

				Set<String> playerInfoKeys = playerInfo.keySet();

				for (String string : playerInfoKeys) {
					newSession.putString(string, playerInfo.get(string).toString());
				}

				System.out.println(
						"newSession after populating the values from playerInfo " + newSession.encodePrettily());
				return newSession;
			} else {
				return new JsonObject().putString("status", "error");
			}

		}
	}

	public boolean validRegisterUserRequest(JsonObject registerUserRequest) {
		// More validation code in the future?
		if (registerUserRequest.containsField("username") && registerUserRequest.containsField("password")
				&& registerUserRequest.containsField("email") && registerUserRequest.containsField("imgData")) {
			if (registerUserRequest.getString("username").trim().equals("")
					|| registerUserRequest.getString("password").trim().equals("")
					|| registerUserRequest.getString("email").trim().equals("")
					|| registerUserRequest.getString("imgData").trim().equals(""))
				return false;
			return true;
		} else
			return false;
	}

	private JsonObject register(JsonObject user) throws InvalidKeySpecException, NoSuchAlgorithmException {
		JsonArray results;
		String username = user.getString("username");
		String password = user.getString("password");
		password = passwordHash.createHash(password);
		String email = user.getString("email");
		String flagUrl = "";
		double level = 0;
		double balance = 50000;
		double streak = 0;
		String usrImg = "assets/images/users/" + username + "/profile_pic.png";
		String imgData = user.getString("imgData");

		JsonObject usernameQuery = new JsonObject().putString("collection", "users")
				.putObject("keys", new JsonObject().putNumber("username", 1))
				.putObject("matcher", new JsonObject().putString("username", username));

		results = dbManager.find(usernameQuery).getArray("results");

		if (results.size() > 0)
			return new JsonObject().putString("status", "error").putString("message",
					"An account was already registered with that username");

		JsonObject emailQuery = new JsonObject().putString("collection", "users")
				.putObject("keys", new JsonObject().putNumber("email", 1))
				.putObject("matcher", new JsonObject().putString("email", email));

		results = dbManager.find(emailQuery).getArray("results");

		if (results.size() > 0)
			return new JsonObject().putString("status", "error").putString("message",
					"An account was already registered with that email");

		JsonObject newUser = new JsonObject().putString("username", username).putString("password", password)
				.putString("email", email).putString("flagUrl", flagUrl).putString("usrImg", usrImg)
				.putNumber("balance", balance).putNumber("streak", streak).putNumber("numStars", 0)
				.putNumber("level", level).putNumber("chipRegenID", -1);

		JsonObject userRegisterRequest = new JsonObject().putString("collection", "users").putObject("document",
				newUser);

		JsonObject result = dbManager.save(userRegisterRequest);

		if (result.getString("status").equals("ok")) {
			createUserDirectory(username);
			saveUserImage(username, imgData);
			eb.send("NewPlayer", newUser);

			/*
			 * TODO: CHIP REGENERATION long chipRegenerator =
			 * vertx.setPeriodic(10000, new Handler<Long>() { public void
			 * handle(Long timerID) {
			 * 
			 * JsonObject getUsername = new JsonObject();
			 * 
			 * getUsername.putString("collection", "users") .putObject("keys",
			 * new JsonObject().putNumber("username", 1)) .putObject("matcher",
			 * new JsonObject().putNumber("chipRegenID", timerID));
			 * 
			 * JsonArray results =
			 * dbManager.find(getUsername).getArray("results");
			 * 
			 * String username =
			 * ((JsonObject)results.get(0)).getString("username");
			 * 
			 * System.out.println(username);
			 * 
			 * JsonObject getBalance = new JsonObject();
			 * 
			 * getBalance.putString("collection", "users") .putObject("keys",
			 * new JsonObject().putNumber("balance", 1)) .putObject("matcher",
			 * new JsonObject().putString("username", username));
			 * 
			 * results = dbManager.find(getBalance).getArray("results");
			 * 
			 * double balance =
			 * ((JsonObject)results.get(0)).getNumber("balance").doubleValue();
			 * 
			 * System.out.println(balance);
			 * 
			 * update(new JsonObject().putString("username",
			 * username).putObject("fields", new
			 * JsonObject().putNumber("balance", balance + 2000)));
			 * 
			 * 
			 * } });
			 * 
			 * update(new JsonObject().putString("username",
			 * username).putObject("fields", new
			 * JsonObject().putNumber("chipRegenID", chipRegenerator)));
			 */

			return new JsonObject().putString("status", "ok");
		} else
			return new JsonObject().putString("status", "error").putString("message", "Something went wrong");
	}

	public boolean validUpdateUserRequest(JsonObject updateUserRequest) {
		// More validation code in the future?
		return updateUserRequest.containsField("username") && updateUserRequest.containsField("fields");
	}

	public JsonObject update(JsonObject updateUserRequest) {
		String username = updateUserRequest.getString("username");
		JsonObject fields = updateUserRequest.getObject("fields");

		System.out.println(updateUserRequest);

		JsonObject updateRequest = new JsonObject().putString("collection", "users")
				.putObject("criteria", new JsonObject().putString("username", username))
				.putObject("objNew", new JsonObject().putObject("$set", fields));

		JsonObject result = dbManager.update(updateRequest);

		System.out.println(result);

		if (result.getString("status").equals("ok")) {

			JsonObject playerDataReq = new JsonObject().putString("collection", "users").putObject("matcher",
					new JsonObject().putString("username", username));

			JsonArray playerData = dbManager.find(playerDataReq).getArray("results");

			JsonObject playerDataAsJsonObject = playerData.get(0);

			eb.publish(EventbusAddress.PlayerUpdates.name(), playerDataAsJsonObject);
		}

		return result;

	}

	public boolean validResumeSessionRequest(JsonObject resumeSessionRequest) {
		// More validation code in the future?
		if (resumeSessionRequest.containsField("sessionID")) {
			return true;
		} else
			return false;
	}

	public JsonObject resume(JsonObject updateUserRequest) {

		String sessionID = updateUserRequest.getString("sessionID");
		if (sessions.get(sessionID) != null) {
			container.logger().info("user details for the session id " + sessions.get(sessionID));
			JsonObject json = new JsonObject(sessions.get(sessionID));

			return json.putString("status", "ok");
		}
		return new JsonObject().putString("status", "error").putString("message", "Not a valid session id");
	}

	public boolean saveUserImage(String username, String imgData) {
		String path = "./webroot/assets/images/users/" + username;
		// String path = "/webroot/assets/images/users/" + username;

		String cadena = System.getProperty("user.dir");
		System.out.println("\r\n*************Dosier d'utilisateur:" + cadena + "\r\n");
		// String path = cadena+"/webroot/assets/images/users/" + username;

		try {
			byte[] imageByte = new byte[65530];
			int posVirgule, posChaine;// On asume le format suivant:
										// "data:img/AAA;base64,DDDD"
			int posPointVirgule; // au AAA est le type de fichier er DDDD les
									// données.
			String chaine = "data:image/";
			String typeDeFichier;
			posVirgule = imgData.indexOf(',');
			posPointVirgule = imgData.indexOf(';');
			posChaine = imgData.indexOf(chaine);
			String extentionDeFichier = new String("png");
			if (posVirgule < 0 || posPointVirgule < 0 || posChaine < 0 || posChaine > posVirgule
					|| posPointVirgule > posVirgule) {
				System.out.println("---SYSTEM: boutisse manquant. On assume type PNG.\r\n");
				System.out.println(">>> " + imgData.subSequence(0, 50) + " <<<\r\n");
				typeDeFichier = new String("png");
				extentionDeFichier = new String("png");
				imageByte = decoder.decodeBuffer(imgData);
			} else {
				typeDeFichier = imgData.substring(posChaine + chaine.length(), posPointVirgule);
				extentionDeFichier = new String(typeDeFichier);
				if (extentionDeFichier.compareToIgnoreCase("png") == 0 && extentionDeFichier.compareTo("png") != 0)
					extentionDeFichier = new String("png");
				System.out.println("+-+-SYSTEM: image reçu (Deplacement aprés de la boutisse): "
						+ imgData.substring(posVirgule + 1));
				imageByte = decoder.decodeBuffer(imgData.substring(posVirgule + 1));
			}
			FileOutputStream file = new FileOutputStream(path + "/profile_pic." + extentionDeFichier);
			file.write(imageByte);
			file.close();
			if (typeDeFichier.compareToIgnoreCase("png") == 0) {

			} else {
				File file2 = new File(path + "/profile_pic." + extentionDeFichier);
				File file3 = new File(path + "/profile_pic.png");
				BufferedImage bufImage = ImageIO.read(file2);
				ImageIO.write(bufImage, extentionDeFichier, file3);
			}
			System.out.println("+++SYSTEM: Creation satisfactoire de image de utilisateur : " + path + "\r\n");
			return true;
		} catch (Exception e) {
			System.out.println(
					"***SYSTEM: Pas possible Creer image de utilizateur: " + path + "\r\n" + e.getMessage() + "\r\n");
			return false;
		}
	}

	public boolean createUserDirectory(String userName) {
		try {
			vertx.fileSystem().mkdirSync("webroot/assets/images/users/" + userName);
			return true;
		} catch (FileSystemException e) {
			System.out.println("error: " + e.getMessage());
			return false;
		}
	}
}
