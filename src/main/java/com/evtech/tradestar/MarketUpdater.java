package com.evtech.tradestar;

import java.text.DecimalFormat;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;

public class MarketUpdater extends Verticle {

	JsonObject currencyList;

	EventBus eb;

	Map<String, Float> currencyMap;

	public void start() {

		System.out.println("MarketUpdater.start()");

		eb = vertx.eventBus();

		currencyMap = vertx.sharedData().getMap("currencyMap");
		CurrencyTypes[] currencies = new CurrencyTypes[12];
		currencies[0] = new CurrencyTypes(1.1078f, "AUD/NZD", 0.0001f);
		currencies[1] = new CurrencyTypes(1.4249f, "EUR/AUD", 0.0005f);
		currencies[2] = new CurrencyTypes(1.3527f, "EUR/USD", 0.0002f);
		currencies[3] = new CurrencyTypes(1.5130f, "GBP/CHF", 0.0003f);
		currencies[4] = new CurrencyTypes(1.6570f, "GBP/USD", 0.00005f);
		currencies[5] = new CurrencyTypes(1.0932f, "USD/CAD", 0.00004f);
		currencies[6] = new CurrencyTypes(0.9120f, "USD/CHF", 0.0002f);
		currencies[7] = new CurrencyTypes(103.527f, "USD/JPY", 0.02f);
		currencies[8] = new CurrencyTypes(1.1078f, "AUD/NZD", 0.0001f);
		currencies[9] = new CurrencyTypes(1.4249f, "EUR/AUD", 0.0005f);
		currencies[10] = new CurrencyTypes(1.3527f, "EUR/USD", 0.0002f);
		currencies[11] = new CurrencyTypes(1.5130f, "GBP/CHF", 0.0003f);

		DecimalFormat df = new DecimalFormat("#.####");

		currencyList = new JsonObject();
		JsonArray arrayForBar;
		float percentage;

		while (true) {

			arrayForBar = new JsonArray();

			for (CurrencyTypes currency : currencies) {
				currency.updateCurrency();

				currencyList.putNumber(currency.getName(), currency.getCurrentRate());
				// if(currency.getName().equals("USD/CAD")){
				// System.out.println(currency.getCurrentRate());
				// }

				percentage = ((currency.getCurrentRate() - currency.getOpeningRate()) / currency.getOpeningRate())
						* 100;

				arrayForBar.add(new JsonObject().putString("currency", currency.getName()).putNumber("volume", 0)
						.putNumber("price", currency.getCurrentRate()).putString("time", "0")
						.putNumber("percentage", percentage));

			}
			vertx.eventBus().publish("RealTimeTrades", currencyList);
			vertx.eventBus().publish("ArrayForBar", arrayForBar);

			for (int i = 0; i < currencies.length; i++) {
				currencyMap.put(currencies[i].getName(), currencies[i].getCurrentRate());
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
